import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
// DTO
import { ClientGroup } from '../../core';

@Injectable({
    providedIn: 'root',
})
export class ClientGroupService {

    private url: string = "http://localhost:4200/api/client-group";

    constructor(private http: HttpClient) { }

    public getAll(): Observable<Array<ClientGroup>> {
        return this.http.get<Array<ClientGroup>>(this.url)
            .pipe(
                catchError(this.handleError('getClientGroup', []))
            );
    }

    public getById(id: number): Observable<ClientGroup> {
        return this.http.get<ClientGroup>(this.url + "/" + id)
            .pipe(
                catchError(this.handleError('getClientGroupById', null))
            );
    }

    public getByIds(id: Array<number>): Observable<Array<ClientGroup>> {
        return this.http.get<Array<ClientGroup>>(this.url + "/" + id)
            .pipe(
                map(group => group.filter((g) => id.indexOf(g.id) !== -1)),
                catchError(this.handleError('getClientGroup', []))
            );
    }

    public search(keyword: string): Observable<Array<ClientGroup>> {
        return this.http.get<Array<ClientGroup>>(this.url + "?name=" + keyword)
            .pipe(
                catchError(this.handleError('seatchClientGroup', []))
            );
    }

    public create(group: ClientGroup): Observable<any> {
        return this.http.post(this.url, group)
            .pipe(
                catchError(this.handleError<any>('createClientGroup'))
            );
    }

    public update(group: ClientGroup): Observable<any> {
        return this.http.put(this.url, group)
            .pipe(
                catchError(this.handleError<any>('updateClientGroup'))
            );
    }

    public delete(group: ClientGroup | number): Observable<any> {
        const id = typeof group === 'number' ? group : group.id;
        return this.http.delete(this.url + "/" + id)
            .pipe(
                catchError(this.handleError<any>('deleteClientGroup'))
            );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            // log to console instead
            console.error(error);

            // TODO: transforming error for user consumption

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}