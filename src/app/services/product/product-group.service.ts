import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
// DTO
import { ProductGroup } from '../../core';

@Injectable({
    providedIn: 'root',
})
export class ProductGroupService {

    private url: string = "http://localhost:4200/api/product-group";

    constructor(private http: HttpClient) { }

    public getAll(): Observable<Array<ProductGroup>> {
        return this.http.get<Array<ProductGroup>>(this.url)
            .pipe(
                catchError(this.handleError('getProductGroup', []))
            );
    }

    public getById(id: string): Observable<ProductGroup> {
        return this.http.get<ProductGroup>(this.url + "/" + id)
            .pipe(
                catchError(this.handleError('getProductGroupById', null))
            );
    }

    public search(keyword: string): Observable<Array<ProductGroup>> {
        return this.http.get<Array<ProductGroup>>(this.url + "?name=" + keyword)
            .pipe(
                catchError(this.handleError('seatchProductGroup', []))
            );
    }

    public create(group: ProductGroup): Observable<any> {
        return this.http.post(this.url, group)
            .pipe(
                catchError(this.handleError<any>('createProductGroup'))
            );
    }

    public update(group: ProductGroup): Observable<any> {
        return this.http.put(this.url, group)
            .pipe(
                catchError(this.handleError<any>('updateProductGroup'))
            );
    }

    public delete(group: ProductGroup | string): Observable<any> {
        const id = typeof group === 'string' ? group : group.id;
        return this.http.delete(this.url + "/" + id)
            .pipe(
                catchError(this.handleError<any>('deleteProductGroup'))
            );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            // log to console instead
            console.error(error);

            // TODO: transforming error for user consumption

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}