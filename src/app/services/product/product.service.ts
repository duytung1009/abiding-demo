import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
// DTO
import { Product } from '../../core';

@Injectable({
    providedIn: 'root',
})
export class ProductService {

    private url: string = "http://localhost:4200/api/products";

    constructor(private http: HttpClient) { }

    public getAll(): Observable<Array<Product>> {
        return this.http.get<Array<Product>>(this.url)
            .pipe(
                catchError(this.handleError('getProduct', []))
            );
    }

    public getById(id: string): Observable<Product> {
        return this.http.get<Product>(this.url + "/" + id)
            .pipe(
                catchError(this.handleError('getProductById', null))
            );
    }

    public search(keyword: string): Observable<Array<Product>> {
        return this.http.get<Array<Product>>(this.url + "?name=" + keyword)
            .pipe(
                catchError(this.handleError('seatchProduct', []))
            );
    }

    public create(group: Product): Observable<any> {
        return this.http.post(this.url, group)
            .pipe(
                catchError(this.handleError<any>('createProduct'))
            );
    }

    public update(group: Product): Observable<any> {
        return this.http.put(this.url, group)
            .pipe(
                catchError(this.handleError<any>('updateProduct'))
            );
    }

    public delete(group: Product | string): Observable<any> {
        const id = typeof group === 'string' ? group : group.id;
        return this.http.delete(this.url + "/" + id)
            .pipe(
                catchError(this.handleError<any>('deleteProduct'))
            );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            // log to console instead
            console.error(error);

            // TODO: transforming error for user consumption

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}