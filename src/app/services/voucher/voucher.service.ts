import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, forkJoin } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
// DTO
import { Voucher, VoucherStatus } from '../../core';
import * as moment from 'moment';
import * as _ from "lodash";

@Injectable({
    providedIn: 'root',
})
export class VoucherService {

    private url: string = "http://localhost:4200/api/vouchers";

    constructor(private http: HttpClient) { }

    public getAll(): Observable<Array<Voucher>> {
        return this.http.get<Array<Voucher>>(this.url)
            .pipe(
                catchError(this.handleError('getVoucher', []))
            );
    }

    public getById(id: number): Observable<Voucher> {
        return this.http.get<Voucher>(this.url + "?id=" + id)
            .pipe(
                catchError(this.handleError('getVoucherById', null))
            );
    }

    public getByIds(ids: Array<number>): Observable<Array<Voucher>> {
        return this.getAll().pipe(
            map(vouchers => _.filter(vouchers, v => _.includes(ids, v.id)))
        );
    }

    public getByStatus(status: VoucherStatus): Observable<Array<Voucher>> {
        return this.http.get<Array<Voucher>>(this.url + "?status=" + status)
            .pipe(
                catchError(this.handleError('getVoucherByStatus', []))
            );
    }

    public search(keyword: string): Observable<Array<Voucher>> {
        return this.http.get<Array<Voucher>>(this.url + "?code=" + keyword)
            .pipe(
                catchError(this.handleError('getVoucherByStatus', []))
            );
    }

    public searchByStatus(keyword: string, status: VoucherStatus): Observable<Array<Voucher>> {
        return this.http.get<Array<Voucher>>(this.url + "?status=" + status + "&code=" + keyword)
            .pipe(
                catchError(this.handleError('getVoucherByStatus', []))
            );
    }

    public create(voucher: Voucher): Observable<any> {
        return this.http.post(this.url, voucher)
            .pipe(
                catchError(this.handleError<any>('createVoucher'))
            );
    }

    public update(voucher: Voucher): Observable<any> {
        return this.http.put(this.url, voucher)
            .pipe(
                catchError(this.handleError<any>('updateVoucher'))
            );
    }

    public startSale(voucher: Voucher): Observable<any> {
        voucher.status = VoucherStatus.RUNNING;
        voucher.startDate = new Date();
        voucher.startDateByDate = moment(new Date()).format('DD/MM/YYYY');
        voucher.startDateByTime = moment(new Date()).format('HH:mm');
        return this.http.put(this.url, voucher)
            .pipe(
                catchError(this.handleError<any>('startSale'))
            );
    }

    public startSales(vouchers: Array<Voucher>): Observable<any> {
        vouchers.forEach(voucher => {
            voucher.status = VoucherStatus.RUNNING;
            voucher.startDate = new Date();
            voucher.startDateByDate = moment(new Date()).format('DD/MM/YYYY');
            voucher.startDateByTime = moment(new Date()).format('HH:mm');
        });
        return forkJoin(vouchers.map(v => this.http.put(this.url, v)));
    }

    public stopSale(voucher: Voucher): Observable<any> {
        voucher.status = VoucherStatus.STOPPED;
        voucher.isEndDateEnable = true;
        voucher.endDate = new Date();
        voucher.endDateByDate = moment(new Date()).format('DD/MM/YYYY');
        voucher.endDateByTime = moment(new Date()).format('HH:mm');
        return this.http.put(this.url, voucher)
            .pipe(
                catchError(this.handleError<any>('stopSale'))
            );
    }

    public stopSales(vouchers: Array<Voucher>): Observable<any> {
        vouchers.forEach(voucher => {
            voucher.status = VoucherStatus.STOPPED;
            voucher.isEndDateEnable = true;
            voucher.endDate = new Date();
            voucher.endDateByDate = moment(new Date()).format('DD/MM/YYYY');
            voucher.endDateByTime = moment(new Date()).format('HH:mm');
        });
        return forkJoin(vouchers.map(v => this.http.put(this.url, v)));
    }

    public delete(voucher: Voucher | number): Observable<any> {
        const id = typeof voucher === 'number' ? voucher : voucher.id;
        return this.http.delete(this.url + "/" + id)
            .pipe(
                catchError(this.handleError<any>('deleteVoucher'))
            );
    }

    public deleteByIds(ids: Array<number>): Observable<any> {
        return forkJoin(ids.map(id => this.http.delete(this.url + "/" + id)));
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            // log to console instead
            console.error(error);

            // TODO: transforming error for user consumption

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}