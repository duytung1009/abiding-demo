import { Component, OnInit, OnDestroy } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Router } from "@angular/router";
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';

import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { Voucher, VoucherStatus } from './../../../../core';
import { VoucherService } from '../../../../services';

import * as _ from "lodash";

@Component({
  selector: 'app-voucher-index',
  templateUrl: './voucher-index.component.html',
  styleUrls: ['./voucher-index.component.css']
})
export class VoucherIndexComponent implements OnInit, OnDestroy {

  private _onDestroy = new Subject();

  vouchers: Array<VoucherItem>;
  checkedId: Array<number>;
  totalChecked: boolean;
  totalIndeterminate: boolean;
  totalCheckedText: string;

  activeTab: number;
  status: VoucherStatus;
  loading: boolean;

  // Search input
  searchInput: FormControl;

  constructor(
    private router: Router,
    private decimalPipe: DecimalPipe,
    private voucherService: VoucherService
  ) {
    this.searchInput = new FormControl("");
    this.checkedId = [];
    this.totalCheckedText = "";
  }

  ngOnInit() {
    const self = this;
    // setup search event
    this.searchInput.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(self._onDestroy)
      ).subscribe(obj => {
        self.searchVoucher(self.searchInput.value);
      });
    this.getVoucher();
  }

  ngOnDestroy() {
    this._onDestroy.next();
  }

  getVoucher(status?: VoucherStatus) {
    const self = this;
    if (this.loading) {
      return;
    }
    if (!status) {
      this.activeTab = 0;
    }
    if (status === VoucherStatus.RUNNING) {
      this.activeTab = 1;
    }
    if (status === VoucherStatus.PENDING) {
      this.activeTab = 2;
    }
    if (status === VoucherStatus.STOPPED) {
      this.activeTab = 3;
    }
    this.loading = true;
    this.searchInput.patchValue("");

    // Get voucher
    if (status === undefined || status === null) {
      this.voucherService.getAll()
        .pipe(takeUntil(self._onDestroy))
        .subscribe(result => {
          self.vouchers = result.map(voucher => new VoucherItem(self.buildMemoString(voucher)));
          if (!self.vouchers || self.vouchers.length === 0) {
            self.redirectToCreatePage();
          }
          self.status = null;
          self.checkedId = [];
          self.totalChecked = false;
          self.totalIndeterminate = false;
          self.totalCheckedText = "";
          self.loading = false;
        });
    } else {
      this.voucherService.getByStatus(status)
        .pipe(takeUntil(self._onDestroy))
        .subscribe(result => {
          self.vouchers = result.map(voucher => new VoucherItem(self.buildMemoString(voucher)));
          self.status = status;
          self.checkedId = [];
          self.totalChecked = false;
          self.totalIndeterminate = false;
          self.totalCheckedText = "";
          self.loading = false;
        });
    }
  }

  checkChange(event: any, voucher: VoucherItem) {
    if (event.checked) {
      this.checkedId.push(voucher.id);
    } else {
      _.remove(this.checkedId, v => v == voucher.id);
    }

    if (this.checkedId.length == this.vouchers.length) {
      // If all element is checked
      this.totalChecked = true;
      this.totalIndeterminate = false;
      this.totalCheckedText = "Đã chọn " + this.checkedId.length + " khuyến mãi";
    } else if (this.checkedId.length == 0) {
      // If none element is checked
      this.totalChecked = false;
      this.totalIndeterminate = false;
      this.totalCheckedText = "";
    } else {
      // If not all element is checked
      this.totalChecked = false;
      this.totalIndeterminate = true;
      this.totalCheckedText = "Đã chọn " + this.checkedId.length + " khuyến mãi";
    }
  }

  totalCheckChange(event: any) {
    this.checkedId = [];
    this.vouchers.forEach(element => {
      element.checked = event.checked;
      if (event.checked) {
        this.checkedId.push(element.id);
      }
    });
    if (event.checked) {
      this.totalCheckedText = "Đã chọn " + this.checkedId.length + " khuyến mãi";
    } else {
      this.totalCheckedText = "";
    }
  }

  redirectToCreatePage() {
    this.router.navigate(['/admin/voucher/create']);
  }

  searchVoucher(keyword: string) {
    const self = this;
    if (this.loading) {
      return;
    }
    this.loading = true;
    if (this.status === undefined || this.status === null) {
      this.voucherService.search(keyword)
        .pipe(takeUntil(self._onDestroy))
        .subscribe(result => {
          self.vouchers = result.map(voucher => new VoucherItem(self.buildMemoString(voucher)));
          self.loading = false;
        });
    } else {
      this.voucherService.searchByStatus(keyword, this.status)
        .pipe(takeUntil(self._onDestroy))
        .subscribe(result => {
          self.vouchers = result.map(voucher => new VoucherItem(self.buildMemoString(voucher)));
          self.loading = false;
        });
    }
  }

  buildMemoString(voucher: Voucher): Voucher {
    let memoString: string = '';

    // Sale unit
    let saleUnit: string = '';
    if (voucher.saleUnit === 0) {
      saleUnit = "%";
    } else {
      saleUnit = "₫";
    }
    // Sale product
    let saleProduct: string = '';
    if (voucher.targetType === 0) {
      saleProduct = "toàn bộ đơn hàng";
    } else if (voucher.targetType === 1) {
      saleProduct = "danh mục ";
      for (let i = 0; i < voucher.targetGroup.length; i++) {
        saleProduct += voucher.targetGroup[i].name;
        if (i != voucher.targetGroup.length - 1) {
          saleProduct += ", ";
        }
      }
    } else {
      saleProduct += voucher.targetProduct.length + " sản phẩm";
    }
    memoString += "Giảm " + this.decimalPipe.transform(voucher.saleValue) + saleUnit + " cho " + saleProduct;

    // Sale limit
    if (voucher.isSaleLowerLimit) {
      memoString += " • Tổng giá trị sản phẩm được khuyến mãi tối thiểu " + this.decimalPipe.transform(voucher.saleLowerLimit) + "₫";
    }

    // Limit by times
    if (voucher.isLimitByTimes) {
      memoString += " • Áp dụng một lần cho mỗi đơn hàng";
    }

    // Client group 
    if (voucher.clientGroup.length > 0) {
      let clientGroup: string = '';
      for (let i = 0; i < voucher.clientGroup.length; i++) {
        clientGroup += voucher.clientGroup[i].name;
        if (i != voucher.clientGroup.length - 1) {
          clientGroup += ", ";
        }
      }
      memoString += " • Áp dụng với nhóm khách hàng " + clientGroup;
    }

    // Limit by client
    if (voucher.isLimitByClient) {
      memoString += " • Một mã trên mỗi khách hàng";
    }

    // Return memoString
    voucher.memoString = memoString;
    return voucher;
  }

  startSale() {
    const self = this;
    this.voucherService.getByIds(this.checkedId)
      .pipe(
        switchMap(vouchers => self.voucherService.startSales(vouchers)),
        takeUntil(self._onDestroy)
      )
      .subscribe(res => {
        self.getVoucher();
      });
  }

  stopSale() {
    const self = this;
    this.voucherService.getByIds(this.checkedId)
      .pipe(
        switchMap(vouchers => self.voucherService.stopSales(vouchers)),
        takeUntil(self._onDestroy)
      )
      .subscribe(vouchers => {
        self.getVoucher();
      });
  }

  deleteVoucher() {
    const self = this;
    this.voucherService.deleteByIds(this.checkedId)
      .pipe(takeUntil(self._onDestroy))
      .subscribe(res => {
        self.getVoucher();
      });
  }

}

export class VoucherItem extends Voucher {

  checked: boolean;

  constructor(voucher: Voucher) {
    super();
    this.id = voucher.id;
    this.code = voucher.code;
    this.link = voucher.link;
    this.memo = voucher.memo;
    this.memoString = voucher.memoString;
    this.status = voucher.status;
    // Sale
    this.saleUnit = voucher.saleUnit;
    this.saleValue = voucher.saleValue;
    this.isSaleLowerLimit = voucher.isSaleLowerLimit;
    this.saleLowerLimit = voucher.saleLowerLimit;
    // Target product
    this.targetType = voucher.targetType;
    this.targetGroup = voucher.targetGroup;
    this.targetProduct = voucher.targetProduct;
    // Client group
    this.clientType = voucher.clientType;
    this.clientGroup = voucher.clientGroup;
    // Usage limit
    this.isLimitByTimes = voucher.isLimitByTimes;
    this.isLimitByClient = voucher.isLimitByClient;
    this.limitCountNumber = voucher.limitCountNumber;
    this.limitMaxNumber = voucher.limitMaxNumber;
    // Time range
    this.startDate = voucher.startDate;
    this.startDateByDate = voucher.startDateByDate;
    this.startDateByTime = voucher.startDateByTime;
    this.endDate = voucher.endDate;
    this.endDateByDate = voucher.endDateByDate;
    this.endDateByTime = voucher.endDateByTime;
    this.isEndDateEnable = voucher.isEndDateEnable;
    // UI 
    this.checked = false;
  }

}
