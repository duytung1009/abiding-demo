import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoucherIndexComponent } from './voucher-index.component';

describe('VoucherIndexComponent', () => {
  let component: VoucherIndexComponent;
  let fixture: ComponentFixture<VoucherIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoucherIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
