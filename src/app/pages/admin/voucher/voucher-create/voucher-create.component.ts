import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { DecimalPipe } from '@angular/common';
import { Subject } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';

import { Voucher, Product, ProductGroup, ClientGroup, TargetType, ClientType } from './../../../../core';
import { VoucherService, ProductService, ProductGroupService, ClientGroupService } from '../../../../services';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as _ from "lodash";
import * as moment from 'moment';

@Component({
  selector: 'app-voucher-create',
  templateUrl: './voucher-create.component.html',
  styleUrls: ['./voucher-create.component.css']
})
export class VoucherCreateComponent implements OnInit, OnDestroy {

  private _onDestroy = new Subject();

  isError: boolean;
  errors: Array<string>;

  isCreate: boolean;
  showAlertUpdated: boolean;
  showAlertStarted: boolean;
  showAlertStopped: boolean;

  voucher: Voucher;
  voucherForm: FormGroup;

  saleChooseList: Array<any>;
  saleChooseUnit: string;
  saleValueDisable: boolean;
  targetTypeList: Array<any>;
  targetSearchDisable: boolean;
  clientTypeList: Array<any>;
  clientSearchDisable: boolean;
  clientType: number;

  productGroupList: Array<ProductGroup>;
  productGroupSelectList: Array<ProductGroup>;
  productList: Array<Product>;
  productSelectList: Array<Product>;
  clientList: Array<ClientGroup>;
  clientSelectList: Array<ClientGroup>;

  modalRef: BsModalRef;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private decimalPipe: DecimalPipe,
    private voucherService: VoucherService,
    private productService: ProductService,
    private productGroupService: ProductGroupService,
    private clientGroupService: ClientGroupService,
    private bsModalService: BsModalService) {
    this.saleChooseList = [
      {
        value: 0,
        text: 'Theo phần trăm'
      },
      {
        value: 1,
        text: 'Theo đơn vị tiền tệ'
      }
    ];
    this.saleChooseUnit = "%";
    this.targetTypeList = [
      {
        value: 0,
        text: 'Tất cả'
      },
      {
        value: 1,
        text: 'Phân nhóm sản phẩm'
      },
      {
        value: 2,
        text: 'Sản phẩm'
      }
    ];
    this.clientTypeList = [
      {
        value: 0,
        text: 'Tất cả'
      },
      {
        value: 1,
        text: 'Nhóm khách hàng đã lưu'
      }
    ];
    this.isError = false;
    this.errors = [];
    this.productGroupSelectList = [];
    this.productSelectList = [];
    this.clientSelectList = [];
  }

  ngOnInit() {
    // Create new instance of Voucher
    this.voucher = new Voucher();
    this.voucher.code = "";
    // Build form
    this.buildForm();
    this.isCreate = (this.router.url.indexOf("/admin/voucher/create") > -1);
    this.activatedRoute.params.forEach((params) => {
      if (params['id']) {
        this.loadVoucher(params['id']);
      }
    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
  }

  loadVoucher(id: number) {
    const self = this;
    this.voucherService.getById(id)
      .pipe(takeUntil(self._onDestroy))
      .subscribe(result => {
        self.voucher = result[0];
        self.genMemo(self.voucher);
        if (self.voucher.targetType == TargetType.GROUP) {
          self.loadProductGroup(self.voucher.targetGroup);
        }
        if (self.voucher.targetType == TargetType.SPECIFIC) {
          self.loadProduct(self.voucher.targetProduct);
        }
        if (self.voucher.clientType == ClientType.GROUP) {
          self.loadClient(self.voucher.clientGroup);
        }      
        self.buildForm();
      });
  }

  buildForm() {
    const self = this;
    if (!this.voucherForm) {
      this.voucherForm = this.fb.group({
        id: [this.voucher.id],
        code: [this.voucher.code, Validators.required],
        link: [this.voucher.link],
        memo: [this.voucher.memo],
        memoString: [this.voucher.memoString],
        status: [this.voucher.status],

        saleUnit: [this.voucher.saleUnit, Validators.required],
        saleValue: [this.voucher.saleValue, Validators.required],
        isSaleLowerLimit: [this.voucher.isSaleLowerLimit],
        saleLowerLimit: [this.voucher.saleLowerLimit],

        targetType: [this.voucher.targetType],
        targetGroup: [this.voucher.targetGroup],
        targetProduct: [this.voucher.targetProduct],

        clientType: [this.voucher.clientType],
        clientGroup: [this.voucher.clientGroup],

        isLimitByTimes: [this.voucher.isLimitByTimes],
        isLimitByClient: [this.voucher.isLimitByClient],
        limitCountNumber: [this.voucher.limitCountNumber ? this.voucher.limitCountNumber : 0],
        limitMaxNumber: [this.voucher.limitMaxNumber],

        isEndDateEnable: [this.voucher.isEndDateEnable],
        startDate: [this.voucher.startDate, Validators.required],
        startDateByDate: [this.voucher.startDateByDate, Validators.required],
        startDateByTime: [this.voucher.startDateByTime, Validators.required],
        endDate: [this.voucher.endDate],
        endDateByDate: [this.voucher.endDateByDate],
        endDateByTime: [this.voucher.endDateByTime],

        productSearchInput: [''],
        clientGroupSearchInput: ['']
      });
    } else {
      let voucher: any = this.voucher;
      voucher.productSearchInput = '';
      voucher.clientGroupSearchInput = '';
      this.voucherForm.setValue(voucher);
    }

    this.saleValueDisable = (!this.voucher.isSaleLowerLimit);
    this.targetSearchDisable = (this.voucher.targetType == TargetType.ALL);
    this.clientSearchDisable = (this.voucher.clientType != ClientType.GROUP);
    (!this.voucher.isSaleLowerLimit) ? this.voucherForm.get('saleLowerLimit').disable() : this.voucherForm.get('saleLowerLimit').enable();
    (!this.voucher.isLimitByTimes) ? this.voucherForm.get('limitMaxNumber').disable() : this.voucherForm.get('limitMaxNumber').enable();
    (this.targetSearchDisable) ? this.voucherForm.get('productSearchInput').disable() : this.voucherForm.get('productSearchInput').enable();
    (this.clientSearchDisable) ? this.voucherForm.get('clientGroupSearchInput').disable() : this.voucherForm.get('clientGroupSearchInput').enable();
    this.voucherForm.valueChanges
      .pipe(takeUntil(self._onDestroy))
      .subscribe(data => self.genMemo(data));
    this.onValueChanged();
  }

  onValueChanged() {
    const self = this;
    this.voucherForm.get('saleUnit').valueChanges
      .pipe(takeUntil(self._onDestroy))
      .subscribe(saleUnit => {
        if (saleUnit == 0) {
          self.saleChooseUnit = "%";
        } else if (saleUnit == 1) {
          self.saleChooseUnit = "₫";
        }
      });
    this.voucherForm.get('isSaleLowerLimit').valueChanges
      .pipe(takeUntil(self._onDestroy))
      .subscribe(isSaleLowerLimit => {
        if (!isSaleLowerLimit) {
          self.voucherForm.get('saleLowerLimit').disable();
          self.saleValueDisable = true;
        }
        else {
          self.voucherForm.get('saleLowerLimit').enable();
          self.saleValueDisable = false;
        }
      });
    this.voucherForm.get('isLimitByTimes').valueChanges
      .pipe(takeUntil(self._onDestroy))
      .subscribe(isLimitByTimes => {
        (!isLimitByTimes) ? self.voucherForm.get('limitMaxNumber').disable() : self.voucherForm.get('limitMaxNumber').enable();
      });
    this.voucherForm.get('targetType').valueChanges
      .pipe(takeUntil(self._onDestroy))
      .subscribe(targetType => {
        if (targetType == TargetType.ALL) {
          self.voucherForm.patchValue({ 'productSearchInput': "" });
          self.removeAllProductGroup();
          self.removeAllProduct();
        }
        if (targetType == TargetType.GROUP) {
          self.voucherForm.patchValue({ 'productSearchInput': "" });
          self.removeAllProduct();
        }
        if (targetType == TargetType.SPECIFIC) {
          self.voucherForm.patchValue({ 'productSearchInput': "" });
          self.removeAllProductGroup();
        }
        self.targetSearchDisable = (targetType == TargetType.ALL);
        (self.targetSearchDisable) ? self.voucherForm.get('productSearchInput').disable() : self.voucherForm.get('productSearchInput').enable();
      });
    this.voucherForm.get('clientType').valueChanges
      .pipe(takeUntil(self._onDestroy))
      .subscribe(clientType => {
        if (clientType == ClientType.ALL) {
          self.voucherForm.patchValue({ 'clientGroupSearchInput': "" });
          self.removeAllClient();
        }
        self.clientSearchDisable = (clientType != ClientType.GROUP);
        (self.clientSearchDisable) ? self.voucherForm.get('clientGroupSearchInput').disable() : self.voucherForm.get('clientGroupSearchInput').enable();
      });
  }

  genCode() {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for (let i = 0; i < 10; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    this.voucher.code = text;
    this.voucherForm.patchValue({ 'code': text });
  }

  genMemo(data: Voucher) {
    let memo: Array<string> = [];

    // Sale unit
    let saleUnit: string = '';
    if (data.saleUnit == 0) {
      saleUnit = "%";
    } else {
      saleUnit = "₫";
    }
    // Sale product
    let saleProduct: string = '';
    if (data.targetType === 0) {
      saleProduct = "toàn bộ đơn hàng";
    } else if (data.targetType === 1) {
      saleProduct = "danh mục ";
      for (let i = 0; i < data.targetGroup.length; i++) {
        saleProduct += data.targetGroup[i].name;
        if (i != data.targetGroup.length - 1) {
          saleProduct += ", ";
        }
      }
    } else {
      saleProduct += data.targetProduct.length + " sản phẩm";
    }
    memo.push("Giảm " + this.decimalPipe.transform(data.saleValue) + saleUnit + " cho " + saleProduct);

    // Sale limit
    if (data.isSaleLowerLimit) {
      memo.push("Tổng giá trị sản phẩm được khuyến mãi tối thiểu " + this.decimalPipe.transform(data.saleLowerLimit) + "₫");
    }

    // Limit by times
    if (data.isLimitByTimes) {
      memo.push("Áp dụng một lần cho mỗi đơn hàng");
    }

    // Client group 
    if (data.clientGroup.length > 0) {
      let clientGroup: string = '';
      for (let i = 0; i < data.clientGroup.length; i++) {
        clientGroup += data.clientGroup[i].name;
        if (i != data.clientGroup.length - 1) {
          clientGroup += ", ";
        }
      }
      memo.push("Áp dụng với nhóm khách hàng " + clientGroup);
    }

    // Limit by client
    if (data.isLimitByClient) {
      memo.push("Một mã trên mỗi khách hàng");
    }

    // Return memoString
    this.voucher.memo = memo;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.bsModalService.show(template, Object.assign({}, { class: 'modal-lg' }));
  }

  onSubmit(event: any) {
    const self = this;
    let voucher: Voucher = this.getVoucher();
    this.clearError();
    // Validate
    if (!this.validate(voucher)) {
      // Show error
      this.isError = true;
      window.scroll(0, 0);
      return;
    }
    if (this.isCreate) {
      // Insert voucher
      this.voucherService.create(voucher)
        .pipe(takeUntil(self._onDestroy))
        .subscribe(res => {
          // Redirect to edit page
          self.router.navigate(['/admin/voucher/update', res.id]);
        });
    } else {
      // Update voucher
      this.voucherService.update(voucher)
        .pipe(takeUntil(self._onDestroy))
        .subscribe(res => {
          window.scroll(0, 0);
          self.loadVoucher(self.voucher.id);
          self.showAlertUpdated = true;
          setTimeout(() => {
            self.showAlertUpdated = false;
          }, 3000);
        });
    }
  }

  startVoucher() {
    const self = this;
    this.voucherService.startSale(this.getVoucher())
      .pipe(takeUntil(self._onDestroy))
      .subscribe(res => {
        self.modalRef.hide();
        self.loadVoucher(self.voucher.id);
        self.showAlertStarted = true;
        setTimeout(() => {
          self.showAlertStarted = false;
        }, 3000);
      });
  }

  stopVoucher() {
    const self = this;
    this.voucherService.stopSale(this.getVoucher())
      .pipe(takeUntil(self._onDestroy))
      .subscribe(res => {
        self.modalRef.hide();
        self.loadVoucher(self.voucher.id);
        self.showAlertStopped = true;
        setTimeout(() => {
          self.showAlertStopped = false;
        }, 3000);
      });
  }

  clearError() {
    this.isError = false;
    this.errors = [];
  }

  validate(voucher: Voucher): boolean {
    if (voucher.startDate > voucher.endDate) {
      this.errors.push('Ngày kết thúc không được nhỏ hơn ngày bắt đầu');
      return false;
    }
    return true;
  }

  getVoucher(): Voucher {
    let voucher: Voucher = this.voucherForm.getRawValue();

    // Gen link (should have been done in server)
    voucher.link = 'http://localhost:4200/' + voucher.code;

    // Handle date
    let momentStartDateByDate = moment(this.voucherForm.get('startDateByDate').value, 'DD/MM/YYYY');
    let momentStartDateByTime = moment(this.voucherForm.get('startDateByTime').value, 'HH:mm');
    let momentStartDate = moment(voucher.startDate);
    momentStartDate.set('date', momentStartDateByDate.date());
    momentStartDate.set('month', momentStartDateByDate.month());
    momentStartDate.set('year', momentStartDateByDate.year());
    momentStartDate.set('hour', momentStartDateByTime.hour());
    momentStartDate.set('minute', momentStartDateByTime.minute());
    let momentEndDateByDate = moment(this.voucherForm.get('endDateByDate').value, 'DD/MM/YYYY');
    let momentEndDateByTime = moment(this.voucherForm.get('endDateByTime').value, 'HH:mm');
    let momentEndDate = moment(voucher.endDate);
    momentEndDate.set('date', momentEndDateByDate.date());
    momentEndDate.set('month', momentEndDateByDate.month());
    momentEndDate.set('year', momentEndDateByDate.year());
    momentEndDate.set('hour', momentEndDateByTime.hour());
    momentEndDate.set('minute', momentEndDateByTime.minute());
    voucher.startDate = momentStartDate.toDate();
    voucher.startDateByDate = momentStartDateByDate.format('DD/MM/YYYY');
    voucher.endDate = momentEndDate.toDate();
    voucher.endDateByDate = momentEndDateByDate.format('DD/MM/YYYY');
    // Calculate status (should have been done in server)
    let isRunning = voucher.isEndDateEnable
      ? moment().isBetween(momentStartDate, momentEndDate)
      : moment().isSameOrAfter(momentStartDate);
    if (isRunning) {
      voucher.status = 0;
    } else {
      if (moment().isBefore(momentStartDate)) {
        voucher.status = 2;
      } else {
        voucher.status = 1;
      }
    }

    console.log(voucher);
    return voucher;
  }

  searchProduct() {
    const self = this;
    let keyword: string = this.voucherForm.get('productSearchInput').value;
    let productType: TargetType = this.voucherForm.get('targetType').value;
    if (productType == TargetType.GROUP) {
      if (keyword == "") {
        this.productGroupList = null;
        return;
      }
      this.productGroupService.search(keyword)
        .pipe(takeUntil(self._onDestroy))
        .subscribe(res => {
          self.productGroupList = res;
        });
    }
    if (productType == TargetType.SPECIFIC) {
      if (keyword == "") {
        this.productList = null;
        return;
      }
      this.productService.search(keyword)
        .pipe(takeUntil(self._onDestroy))
        .subscribe(res => {
          self.productList = res;
        });
    }
  }

  searchClientGroup() {
    const self = this;
    let keyword: string = this.voucherForm.get('clientGroupSearchInput').value;
    if (keyword == "") {
      this.clientList = null;
      return;
    }
    this.clientGroupService.search(keyword)
      .pipe(takeUntil(self._onDestroy))
      .subscribe(res => {
        self.clientList = res;
      });
  }

  /**
   * Client Group method
   * @param clientGroup 
   */
  loadClient(clientGroup: Array<ClientGroup>) {
    this.clientSelectList = clientGroup;
  }

  addClient(client: ClientGroup) {
    this.clientSelectList.push(client);
    this.clientSelectList = _.uniqBy(this.clientSelectList, c => c.id);
    this.voucherForm.patchValue({ 'clientGroup' : this.clientSelectList });
    this.clientList = null;
  }

  removeClient(id: string) {
    _.remove(this.clientSelectList, c => c.id == id);
  }

  removeAllClient() {
    this.clientList = null;
    this.clientSelectList = [];
    this.voucherForm.patchValue({ 'clientGroup' : this.clientSelectList });
  }

  /**
   * Product Group method
   * @param productGroup 
   */
  loadProductGroup(productGroup: Array<ProductGroup>) {
    this.productGroupSelectList = productGroup;
  }

  addProductGroup(productGroup: ProductGroup) {
    this.productGroupSelectList.push(productGroup);
    this.productGroupSelectList = _.uniqBy(this.productGroupSelectList, c => c.id);
    this.voucherForm.patchValue({ 'targetGroup' : this.productGroupSelectList });
    this.productGroupList = null;
  }

  removeProductGroup(id: string) {
    _.remove(this.productGroupSelectList, c => c.id == id);
  }

  removeAllProductGroup() {
    this.productGroupList = null;
    this.productGroupSelectList = [];
    this.voucherForm.patchValue({ 'targetGroup' : this.productGroupSelectList });
  }

  /**
   * Product method
   * @param product 
   */
  loadProduct(product: Array<Product>) {
    this.productSelectList = product;
  }

  addProduct(product: Product) {
    this.productSelectList.push(product);
    this.productSelectList = _.uniqBy(this.productSelectList, c => c.id);
    this.voucherForm.patchValue({ 'targetProduct' : this.productSelectList });
    this.productList = null;
  }

  removeProduct(id: string) {
    _.remove(this.productSelectList, c => c.id == id);
  }

  removeAllProduct() {
    this.productList = null;
    this.productSelectList = [];
    this.voucherForm.patchValue({ 'targetProduct' : this.productSelectList });
  }

}
