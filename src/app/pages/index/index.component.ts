import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Product, ProductGroup, ClientGroup } from '../../core';
import { ProductService, ProductGroupService, ClientGroupService } from '../../services';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {

  private _onDestroy = new Subject();

  clientGroups: Array<ClientGroup>;
  productGroups: Array<ProductGroup>;
  products: Array<Product>;

  constructor(
    private clientGroupService: ClientGroupService,
    private productGroupService: ProductGroupService,
    private productService: ProductService) { }

  ngOnInit() {
    this.getClientGroup();
    this.getProductGroup();
    this.getProduct();
  }

  ngOnDestroy() {
    this._onDestroy.next();
  }

  getClientGroup() {
    const self = this;
    this.clientGroupService.getAll()
      .pipe(takeUntil(self._onDestroy))
      .subscribe(result => self.clientGroups = result);
  }

  getProductGroup() {
    const self = this;
    this.productGroupService.getAll()
      .pipe(takeUntil(self._onDestroy))
      .subscribe(result => self.productGroups = result);
  }

  getProduct() {
    const self = this;
    this.productService.getAll()
      .pipe(takeUntil(self._onDestroy))
      .subscribe(result => self.products = result);
  }

}
