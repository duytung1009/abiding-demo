export class Product {
    id: string;
    groupId: string;
    name: string;
    thumbnailUrl: string;
}
