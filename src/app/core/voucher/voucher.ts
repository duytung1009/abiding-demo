import { ClientType } from './client-type.enum';
import { SaleUnit } from './sale-unit.enum';
import { TargetType } from './target-type.enum';
import { VoucherStatus } from './voucher-status.enum';
import { ClientGroup } from '../client';
import { Product, ProductGroup } from '../product';

import * as moment from 'moment';

export class Voucher {
    id: number;
    code: string;
    link: string;
    memo: Array<string>;
    memoString: string;
    status: VoucherStatus;
    // Sale
    saleUnit: SaleUnit;
    saleValue: number;
    isSaleLowerLimit: boolean;
    saleLowerLimit: number;
    // Target product
    targetType: TargetType;
    targetGroup: Array<ProductGroup>;
    targetProduct: Array<Product>;
    // Client group
    clientType: ClientType;
    clientGroup: Array<ClientGroup>;
    // Usage limit
    isLimitByTimes: boolean;
    isLimitByClient: boolean;
    limitCountNumber: number;
    limitMaxNumber: number;
    // Time range
    startDate: Date;
    startDateByDate: any
    startDateByTime: any;
    endDate: Date;
    endDateByDate: any;
    endDateByTime: any;
    isEndDateEnable: boolean;

    constructor() {
        this.saleUnit = SaleUnit.PERCENT;
        this.targetType = TargetType.ALL;
        this.clientType = ClientType.ALL;
        this.isSaleLowerLimit = false;
        this.isLimitByTimes = false;
        this.isLimitByClient = false;
        this.isEndDateEnable = false;
        this.targetType = TargetType.ALL;
        this.clientType = ClientType.ALL;
        this.startDate = new Date();
        this.startDateByDate = moment(this.startDate).format('DD/MM/YYYY');
        this.startDateByTime = moment(this.startDate).format('HH:mm');
        this.endDate = new Date();
        this.endDateByDate = moment(this.endDate).format('DD/MM/YYYY');
        this.endDateByTime = moment(this.endDate).format('HH:mm');
        this.targetGroup = [];
        this.targetProduct = [];
        this.clientGroup = [];
    }
}
