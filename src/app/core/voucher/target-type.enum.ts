export enum TargetType {
    ALL = 0,
    GROUP,
    SPECIFIC
}