import { InMemoryDbService } from 'angular-in-memory-web-api';
import * as moment from 'moment';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        const clientGroup = [
            { id: 1, name: 'Khách hàng thân thiết' },
            { id: 2, name: 'Khách hàng sử dụng thẻ tín dụng' },
            { id: 3, name: 'Khách hàng mới' }
        ];
        const productGroup = [
            { id: '1', name: 'Đồ gia dụng' },
            { id: '2', name: 'Laptop - Thiết bị IT' },
            { id: '3', name: 'Điện thoại - Máy tính bảng' }
        ];
        const products = [
            { id: '1', groupId: '1', name: 'Bộ chén đĩa thủy tinh Prometeo Bormioli Rocco -10 Món', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/media/catalog/product/b/o/bo-chen-dia-thuy-tinh-10-mon-prometeo-combotrometeo10_-_1_1_.jpg' },
            { id: '2', groupId: '1', name: 'Máy làm sữa đậu nành BLUESTONE SMB-7358 (1.3L)', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/14/64/fe/9a920f91245eb9ce02d3d8adc11f49e1.jpg' },
            { id: '3', groupId: '1', name: 'Dao cưa Hobby Saw nội địa Nhật Bản', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/3b/c9/d0/ff304cb3a230c9a994ef1b5df6986f5e.jpg' },
            { id: '4', groupId: '2', name: 'Laptop Asus VivoBook S15 S510UQ-BQ483T Core i7-8550U / Win 10 15.6 inch', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/b9/57/b9/a4f6b0b786bccc28d74177c47e8c4496.jpg' },
            { id: '5', groupId: '2', name: 'MacBook Pro Touch Bar 2018 MR9Q2 Core i5/256GB (13 inch) (Space Gray) - Hàng Chính Hãng', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/59/b3/77/9ed9410edc9cc41c8064e87c1cb00f50.jpg' },
            { id: '6', groupId: '2', name: 'Màn Hình Di Động Asus ZenScreen MB16AC 16inch Full HD 5ms 60Hz IPS USB Type-C - Hàng Chính Hãng', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/44/b1/30/f12c0cfe19825ebd84b62392f0602307.jpg' },
            { id: '7', groupId: '2', name: 'Máy In Laser Đen Trắng Đơn Năng Brother HL-L2321D', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/d7/e0/e6/b4042c269537274c60a916cde66f75b7.jpg' },
            { id: '8', groupId: '3', name: 'Điện Thoại iPhone X 64GB VN/A - Hàng Chính Hãng', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/f9/39/c2/4657173e912f7b7d8a6591cf1f0524a9.jpg' },
            { id: '9', groupId: '3', name: 'Điện Thoại Samsung Galaxy Note 8 - Hàng Chính Hãng', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/c2/c4/62/970319156e63512f408b5de72657d5e7.jpg' },
            { id: '10', groupId: '3', name: 'Điện Thoại Xiaomi Mi Mix 2 (Black) - Hàng Chính Hãng', thumbnailUrl: 'https://vcdn.tikicdn.com/cache/75x75/ts/product/cc/56/db/04d361740c7cae04a0fd37cb002b199f.jpg' }
        ];
        const date1 = new Date(2018, 5, 3, 0, 0, 0);
        const date2 = new Date(2018, 6, 3, 0, 0, 0);
        const date3 = new Date(2018, 9, 10, 0, 0, 0);
        const date4 = new Date(2018, 11, 25, 0, 0, 0);
        const dateByDate1 = moment(date1).format('DD/MM/YYYY');
        const dateByDate2 = moment(date2).format('DD/MM/YYYY');
        const dateByDate3 = moment(date3).format('DD/MM/YYYY');
        const dateByDate4 = moment(date4).format('DD/MM/YYYY');
        const dateByTime1 = moment(date1).format('HH:mm');
        const dateByTime2 = moment(date2).format('HH:mm');
        const dateByTime3 = moment(date3).format('HH:mm');
        const dateByTime4 = moment(date4).format('HH:mm');
        const vouchers = [
            {
                id: 1,
                code: 'SOZWB79OZC2E',
                link: 'http://localhost:4200/SOZWB79OZC2E',
                memo: [],
                memoString: '',
                status: 1,
                // Sale
                saleUnit: 1,
                saleValue: 2,
                isSaleLowerLimit: true,
                saleLowerLimit: 1000000,
                // Target product
                targetType: 1,
                targetGroup: [productGroup.find(p => p.id == '1')],
                targetProduct: [],
                // Client group
                clientType: 0,
                clientGroup: [],
                // Usage limit
                isLimitByTimes: true,
                isLimitByClient: true,
                limitCountNumber: 0,
                limitMaxNumber: 10,
                // Time range
                startDate: date1,
                startDateByDate: dateByDate1,
                startDateByTime: dateByTime1,
                endDate: date2,
                endDateByDate: dateByDate2,
                endDateByTime: dateByTime2,
                isEndDateEnable: true,
            },
            {
                id: 2,
                code: 'N3ASTN2FQXDS',
                link: 'http://localhost:4200/N3ASTN2FQXDS',
                memo: [],
                memoString: '',
                status: 2,
                // Sale
                saleUnit: 1,
                saleValue: 200000,
                isSaleLowerLimit: true,
                saleLowerLimit: 2500000,
                // Target product
                targetType: 0,
                targetGroup: [],
                targetProduct: [],
                // Client group
                clientType: 0,
                clientGroup: [],
                // Usage limit
                isLimitByTimes: false,
                isLimitByClient: true,
                limitCountNumber: 0,
                limitMaxNumber: 20,
                // Time range
                startDate: date3,
                startDateByDate: dateByDate3,
                startDateByTime: dateByTime3,
                endDate: date4,
                endDateByDate: dateByDate4,
                endDateByTime: dateByTime4,
                isEndDateEnable: true,
            },
            {
                id: 3,
                code: 'TBVSTN0S18',
                link: 'http://localhost:4200/TBVSTN0S18',
                memo: [],
                memoString: '',
                status: 0,
                // Sale
                saleUnit: 0,
                saleValue: 5,
                isSaleLowerLimit: true,
                saleLowerLimit: 2000000,
                // Target product
                targetType: 2,
                targetGroup: [],
                targetProduct: products.filter(p => (p.id == '8' || p.id == '9')),
                // Client group
                clientType: 1,
                clientGroup: [clientGroup.find(c => c.id == 1)],
                // Usage limit
                isLimitByTimes: false,
                isLimitByClient: true,
                limitCountNumber: 0,
                limitMaxNumber: 10,
                // Time range
                startDate: date2,
                startDateByDate: dateByDate2,
                startDateByTime: dateByTime2,
                endDate: date3,
                endDateByDate: dateByDate3,
                endDateByTime: dateByTime3,
                isEndDateEnable: true,
            }
        ];
        return {
            'client-group': clientGroup,
            'product-group': productGroup,
            'products': products,
            'vouchers': vouchers,
        };
    }
}