import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule, MatRadioModule } from '@angular/material';
import { DecimalPipe } from '@angular/common';

// In-memory Web API
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

// i18n
import { registerLocaleData } from '@angular/common';
import localeVi from '@angular/common/locales/vi';
registerLocaleData(localeVi, 'vi');

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { VoucherIndexComponent } from './pages/admin/voucher/voucher-index/voucher-index.component';
import { VoucherCreateComponent } from './pages/admin/voucher/voucher-create/voucher-create.component';
import { HeaderComponent } from './pages/included/header/header.component';
import { SidebarComponent } from './pages/included/sidebar/sidebar.component';
import { IndexComponent } from './pages/index/index.component';

// 3rd 
import { MomentModule } from 'angular2-moment/moment.module';
import { ClipboardModule } from 'ngx-clipboard';
import { ModalModule, AlertModule, BsDatepickerModule, BsDropdownModule, PopoverModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    VoucherIndexComponent,
    VoucherCreateComponent,
    HeaderComponent,
    SidebarComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { 
        dataEncapsulation: false,
        apiBase: 'api/'
      }
    ),
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatRadioModule,
    AppRoutingModule,
    MomentModule,
    ClipboardModule,
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    PopoverModule.forRoot()
  ],
  providers: [
    DecimalPipe,
    { provide: LOCALE_ID, useValue: 'vi' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
