import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { IndexComponent } from './pages/index/index.component';
import { VoucherIndexComponent } from './pages/admin/voucher/voucher-index/voucher-index.component';
import { VoucherCreateComponent } from './pages/admin/voucher/voucher-create/voucher-create.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', component: IndexComponent },
      { path: 'admin/voucher', component: VoucherIndexComponent },
      { path: 'admin/voucher/create', component: VoucherCreateComponent },
      { path: 'admin/voucher/update/:id', component: VoucherCreateComponent }
    ])
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
