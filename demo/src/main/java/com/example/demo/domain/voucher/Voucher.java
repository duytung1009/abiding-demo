package com.example.demo.domain.voucher;

import java.util.Date;
import java.util.List;

import com.example.demo.domain.client.ClientGroup;
import com.example.demo.domain.product.Product;
import com.example.demo.domain.product.ProductGroup;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Voucher {

	private Integer id;

	private String code;

	private String link;

	private List<String> memo;

	private String memoString;

	private VoucherStatus status;

	// Sale
	private SaleUnit saleUnit;

	private Integer saleValue;

	private Boolean isSaleLowerLimit;

	private Integer saleLowerLimit;

	// Product product
	private ProductType productType;

	private List<ProductGroup> productGroupList;

	private List<Product> productList;

	// Client group
	private ClientType clientType;

	private List<ClientGroup> clientGroup;

	// Usage limit
	private Boolean isLimitByTimes;

	private Boolean isLimitByClient;

	private Integer limitCountNumber;

	private Integer limitMaxNumber;

	// Time range
	private Boolean isEndDateEnable;

	private Date startDate;

	private Date endDate;
}
