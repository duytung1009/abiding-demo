package com.example.demo.domain.voucher;

public enum SaleUnit {
	PERCENT, 
	CURRENCY
}
