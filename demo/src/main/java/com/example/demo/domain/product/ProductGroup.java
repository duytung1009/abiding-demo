package com.example.demo.domain.product;

import lombok.Data;

@Data
public class ProductGroup {

	private String id;
	
	private String name;
	
}
