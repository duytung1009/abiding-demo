package com.example.demo.domain.voucher;

public enum VoucherStatus {
	RUNNING,
	STOPPED,
	PENDING
}
