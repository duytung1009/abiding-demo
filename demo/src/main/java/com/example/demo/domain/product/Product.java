package com.example.demo.domain.product;

import lombok.Data;

@Data
public class Product {
	
	private String id;
	
	private String groupId;
	
	private String name;
	
	private String thumbnailUrl;
	
}
