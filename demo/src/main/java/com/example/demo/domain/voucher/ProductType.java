package com.example.demo.domain.voucher;

public enum ProductType {
	ALL, 
	GROUP, 
	SPECIFIC
}
