package com.example.demo.domain.client;

import lombok.Data;

@Data
public class ClientGroup {

	private Integer id;
	
	private String name;
	
}
