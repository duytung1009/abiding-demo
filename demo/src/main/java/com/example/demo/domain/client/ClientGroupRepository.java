package com.example.demo.domain.client;

import java.util.List;

public interface ClientGroupRepository {

	List<ClientGroup> getAll();
	
	ClientGroup getById(Integer id);
	
	List<ClientGroup> getByIds(List<Integer> ids);
	
	List<ClientGroup> searchByName(String keyword);
	
}
