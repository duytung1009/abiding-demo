package com.example.demo.infra.client;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.client.ClientGroup;
import com.example.demo.domain.client.ClientGroupRepository;

@Repository
public class ClientGroupRepositoryImpl implements ClientGroupRepository {

	@Autowired
	private ClientGroupJpaRepository clientGroupJpaRepo;

	@Override
	public List<ClientGroup> getAll() {
		return this.clientGroupJpaRepo.findAll().stream()
				.map(this::toDomain)
				.collect(Collectors.toList());
	}

	@Override
	public ClientGroup getById(Integer id) {
		return this.toDomain(this.clientGroupJpaRepo.findById(id).orElse(null));
	}

	@Override
	public List<ClientGroup> getByIds(List<Integer> ids) {
		return this.clientGroupJpaRepo.findByIdIn(ids).stream()
				.map(this::toDomain)
				.collect(Collectors.toList());
	}

	@Override
	public List<ClientGroup> searchByName(String keyword) {
		return this.clientGroupJpaRepo.findByNameContaining(keyword).stream()
				.map(this::toDomain)
				.collect(Collectors.toList());
	}
	
	private ClientGroup toDomain(ClientGroupEntity entity) {
		if (entity == null) {
			return null;
		}
		ClientGroup domain = new ClientGroup();
		domain.setId(entity.getId());
		domain.setName(entity.getName());
		return domain;
	}

}
