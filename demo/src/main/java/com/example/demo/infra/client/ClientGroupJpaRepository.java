package com.example.demo.infra.client;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientGroupJpaRepository extends JpaRepository<ClientGroupEntity, Integer> {

	List<ClientGroupEntity> findByIdIn(List<Integer> ids);
	
	List<ClientGroupEntity> findByNameContaining(String name); 
	
}
