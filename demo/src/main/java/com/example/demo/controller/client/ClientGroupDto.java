package com.example.demo.controller.client;

import com.example.demo.domain.client.ClientGroup;

import lombok.Data;

@Data
public class ClientGroupDto {

	private Integer id;

	private String name;
	
	public static ClientGroupDto toDto(ClientGroup domain) {
		ClientGroupDto dto = new ClientGroupDto();
		dto.setId(domain.getId());
		dto.setName(domain.getName());
		return dto;
	}
	
	public ClientGroup toDomain() {
		ClientGroup domain = new ClientGroup();
		domain.setId(this.getId());
		domain.setName(this.getName());
		return domain;
	}

}
