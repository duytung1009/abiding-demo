package com.example.demo.controller.client;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.client.ClientGroupRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/client-group")
public class ClientGroupController {

	@Autowired
	private ClientGroupRepository clientGroupRepo;

	@GetMapping("/get")
	public List<ClientGroupDto> getAll() {
		return this.clientGroupRepo.getAll().stream()
				.map(ClientGroupDto::toDto)
				.collect(Collectors.toList());
	}
	
	@GetMapping("/get/{id}")
	public ClientGroupDto getById(@PathVariable("id") Integer id) {
		return ClientGroupDto.toDto(this.clientGroupRepo.getById(id));
	}
	
	@GetMapping("/get-batch/{ids}")
	public List<ClientGroupDto> getById(@PathVariable("ids") List<Integer> ids) {
		return this.clientGroupRepo.getByIds(ids).stream()
				.map(ClientGroupDto::toDto)
				.collect(Collectors.toList());
	}
	
	@GetMapping("/search/{keyword}")
	public List<ClientGroupDto> search(@PathVariable("keyword") String keyword) {
		return this.clientGroupRepo.searchByName(keyword).stream()
				.map(ClientGroupDto::toDto)
				.collect(Collectors.toList());
	}

}
